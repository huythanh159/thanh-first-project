
document.addEventListener("DOMContentLoaded", function () {//event nay la web se load cho minh
    document.getElementById("read-csv").addEventListener("click", function () {//khi minh click
        file = document.getElementById("upload-csv").files[0];
        var reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function () {
            var contentFile = reader.result;
            var Arr = contentFile.split("\n");
            var ArrHeader = Arr[0].split(",");
            
            
            console.log(Arr)
            var arrContent = [];
            for (let index = 1; index < Arr.length; index++) {
                var arrs = Arr[index].split(",");
                //console.log(arrs)
                arrContent.push({
                    id: arrs[0],
                    revenue: arrs[1],
                    profit: arrs[2],
                    cost: arrs[3],
                })
            }
            console.log(arrContent)
            //console.log(arrContent)
            var table = "<table>";
            table += "<th>"
            table += "ID"
            table += "</th>"
            table += "<th>"
            table += "Revenue"
            table += "</th>"
            table += "<th>"
            table += "Profit"
            table += "</th>"
            table += "<th>"
            table += "Cost"
            table += "</th>"
            arrContent.forEach(element => {
                table += "<tr>"
                table += "<td>"
                table += element.id
                table += "</td>"
                table += "<td>"
                table += element.revenue
                table += "</td>"
                table += "<td>"
                table += element.profit
                table += "</td>"
                table += "<td>"
                table += element.cost
                table += "</td>"

                table += "</tr>"
            })
            table += "</table>";
            document.getElementById("table-data").innerHTML = table;

            //bieu dien char
            var canvasElement = document.getElementById("myChart");
            const data = {
                labels: ArrHeader,
                datasets: [{
                    label: '',
                    data: [
                        arrContent[0].id,
                        arrContent[0].revenue,
                        arrContent[0].profit,
                        arrContent[0].cost
                    ],
                    backgroundColor: "red"
                    ,
                    borderColor: 
                        'rgba(255, 26, 104, 12)'
                    ,
                    borderWidth: 1
                },
                {
                    label: '',
                    data: [
                        arrContent[1].id,
                        arrContent[1].revenue,
                        arrContent[1].profit,
                        arrContent[1].cost
                    ],
                    backgroundColor: "blue",
                        
                    borderColor: 
                        'rgba(255, 26, 104, 1)',
                    borderWidth: 1
                },
                {
                    label: '',
                    data: [
                        arrContent[2].id,
                        arrContent[2].revenue,
                        arrContent[2].profit,
                        arrContent[2].cost
                    ],
                    backgroundColor: "yellow",
                    borderColor: 
                        'rgba(255, 26, 104, 1)',
                    borderWidth: 1
                }]
            };
            // config 
            const config = {
                type: 'bar',
                data,
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            };
            var myChar = new Chart(canvasElement, config);
        }
    })
})